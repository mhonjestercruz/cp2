let registerForms = document.querySelector('#registerUser');
// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

registerForms.addEventListener('submit', (e) => {
    e.preventDefault();
    let firstName = document.querySelector('#firstName').value;
    let lastName = document.querySelector('#lastName').value;
    let mobileNumber = document.querySelector('#mobileNumber').value;
    let userEmail = document.querySelector('#userEmail').value;
    let password1 = document.querySelector('#password1').value;
    let verifyPass = document.querySelector('#password2').value;

    if ((password1 !== '' && verifyPass !== '') && (password1 === verifyPass) && (mobileNumber.length === 11)) {
        // check the database
        // check for duplicate email in database first
        // url-where we can get the duplicate routes in our server
        // asynchronously load or fetch content from url
        fetch('https://young-earth-66670.herokuapp.com/api/users/email-exists', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                'email': userEmail
            })
        })
        // return a promise that resolves when the result is loaded
        .then(result => result.json())//call this function when result is loaded
        .then(data => {
            if (data === true) return alert('Duplicate Email');
            //if no duplicates found
            //get the routes for registration in our server
            fetch('https://young-earth-66670.herokuapp.com/api/users/', {
                method: 'POST',
                headers: {
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    'firstName': firstName,
                    'lastName': lastName,
                    'email': userEmail,
                    'mobileNo': mobileNumber,
                    'password': password1
                })
            })
            .then(result => result.json())
            .then(data => {
                console.log(data)
                //if registration is successful
                if (data === true) {
                    alert('Registered Successfully')
                    window.location.replace('./login.html')
                } else {
                    //error occured in registration
                    alert('Something went wrong!')
                }
            })
        })
    } else {
        alert('something went wrong, please try again');
    }
});

// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);



