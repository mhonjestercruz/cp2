// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

let editCourses = document.getElementById('editCourse');
const myToken = localStorage.getItem('token');
// console.log(myToken)
let params = new URLSearchParams(window.location.search);
let courseIds = params.get('courseId');


    let courseName = document.querySelector('#courseName');
    let coursePrice = document.querySelector('#coursePrice');
    let courseDescription = document.querySelector('#courseDescription');

    
    fetch(`https://young-earth-66670.herokuapp.com/api/courses/${courseIds}`)
    .then(result => result.json())
    .then(data => {
        if (!data) return console.log('Something went wrong')
        else {
            courseName.placeholder = data.name
            coursePrice.placeholder = `₱ ${data.price}`
            courseDescription.placeholder = data.description
            console.log()
        }
        editCourses.addEventListener('submit', (e) => {
        e.preventDefault();
        if (courseName.value === '' || coursePrice.value === '' || courseDescription.value === '') {
            return alert('Invalid Input')
        }
        
        fetch(`https://young-earth-66670.herokuapp.com/api/courses/${courseIds}`, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json',
                Authorization: `Bearer ${myToken}`
            },
            body: JSON.stringify({
                name: courseName.value,
                price: coursePrice.value,
                description: courseDescription.value
            })
        })
        .then(res => res.json())
        .then(data => {
            if (data === 'false') return alert('Something went wrong')
            else {
                window.location.replace('../pages/courses.html')
                return alert('Course is successfully updated')
            }
        })
    })
})


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);