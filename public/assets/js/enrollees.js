// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

let isAdmin = localStorage.getItem('isAdmin')
let token = localStorage.getItem('token');
let userProfile = document.querySelector('#profileContainer');
let enrollees

if (!token) alert('Something went wrong')
if (isAdmin === 'false') alert('You are not valid to access this site')
else {
    fetch('https://young-earth-66670.herokuapp.com/api/users/allEnrolled')
    .then(res => res.json())
    .then(data => {
        if (!data) alert('Something went wrong')
        let allData
        allData = data.map(enrollees => {
            
            return (
                `
                <div class="col-md-12 my-5 jumbotron">
                    <div class="card">
                        <div class="card-body">
                            <p class="text-center font-weight-bold">
                                Student ID: ${enrollees._id}
                            </p>
                            <p class="text-center font-weight-bold">
                                Student Name: ${enrollees.firstName} ${enrollees.lastName}
                            </p>
                            <p class="text-center font-weight-bold">
                                Email: ${enrollees.email}
                            </p>
                            <p class="text-center font-weight-bold">
                                Mobile Number: ${enrollees.mobileNo}
                            </p>
                        </div>
                    </div>
                </div>
                `
            ) 
        }).join('')
        userProfile.innerHTML = allData
    })

    
}


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);
