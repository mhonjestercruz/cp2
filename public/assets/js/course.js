// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

const mySingleToken = localStorage.getItem("token");
let params = new URLSearchParams(window.location.search);

// console.log(params)
let myCourseId = params.get('courseId');
// console.log(courseIds)

const courseName = document.getElementById('courseName');
const coursePrice = document.getElementById('coursePrice');
const courseDescrip = document.getElementById('courseDesc');
const enrollContainer = document.getElementById('enrollContainer')

// console.log(enrollButton)

fetch(`https://young-earth-66670.herokuapp.com/api/courses/${myCourseId}`)
    .then(res => res.json())
    .then(data => {
        if (data) {
            courseName.innerHTML = data.name
            coursePrice.innerHTML = data.price;
            courseDescrip.innerHTML = data.description
            enrollContainer.innerHTML = 
                `
                <button href="#" value={data._id} class="btn mt-5 btn-danger btn-block" id="enrollButton">Enroll
                </button>
                `

            let enrollButton = document.querySelector('#enrollButton');
            enrollButton.addEventListener('click', (e) => {
                e.preventDefault()
                fetch('https://young-earth-66670.herokuapp.com/api/users/enroll', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        Authorization: `Bearer ${mySingleToken}`
                    },
                    body: JSON.stringify({
                        "courseId": myCourseId
                    })
                }).then(res =>  res.json())
                .then(data => {
                    if (!data) return alert('Something went wrong, Please try again!')
                    alert('Thank you for enrolling! We\'re excited to meet you soon!')
                    window.location.replace('./profile.html')
                })
            
            })
        } else {
            alert('No such data found!')
        }
})


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);
