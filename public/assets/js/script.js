let navitems = document.querySelector('#navSession');
// console.log(navitems);

// let bookNow = document.querySelector('#bookNow');
const myAdmin = localStorage.getItem('isAdmin')
let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {
    navitems.innerHTML = 
        `
        <a href="../pages/login.html" class="button">Enroll Now!</a>
        `
} else if (myAdmin == 'true') {
    navitems.innerHTML =
    `
    <li class ="navbar__item">
        <a href="./enrollees.html" class="navbar__link">Enrollees</a>
    </li>
    <li class ="navbar__item">
        <a href="./logout.html" class="navbar__link" id="logoutButton">Logout</a>
    </li>`
} else {
    navitems.innerHTML =
    `
    <li class ="navbar__item">
        <a href="./profile.html" class="navbar__link">Profile</a>
    </li>
    <li class ="navbar__item">
        <a href="./logout.html" class="navbar__link" id="logoutButton">Logout</a>
    </li>`
}

const myLogout = document.querySelector('#logoutButton');

myLogout.addEventListener('click', () => {
    if (userToken) {
        localStorage.clear();
    }
})