// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

const myAdmin = localStorage.getItem('isAdmin')
const bookNow__button = document.querySelector('#bookNow__button');
let login = document.querySelector('#login');
// console.log(navitems);

// let bookNow = document.querySelector('#bookNow');

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {
    login.innerHTML = 
        `
        <a href="./pages/login.html" class="button">Enroll Now!</a>
        `
} else if (myAdmin == 'true') {
    login.innerHTML =
    `
    <li class="navbar__item">
        <a href="./pages/enrollees.html" class="navbar__link">Enrollees</a>
    </li>
    <li class="navbar__item">
        <a href="./pages/logout.html" class="navbar__link" id="logoutButton">Logout</a>
    </li>`
} else {
    login.innerHTML =
    `
    <li class="navbar__item">
        <a href="./pages/profile.html" class="navbar__link">Profile</a>
    </li>
    <li class="navbar__item">
        <a href="./pages/logout.html" class="navbar__link" id="logoutButton">Logout</a>
    </li>`
    
}

let logout = document.querySelector('#logoutButton');

if (!userToken) {
    bookNow__button.innerHTML = 
    `
    <a href="./pages/login.html" class="overlay__button">Book Now!</a>
    `
} else {
    bookNow__button.innerHTML = 
    `
    <a href="./pages/courses.html" class="overlay__button">Book Now!</a>
    `
}


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);


logout.addEventListener('click', () => {
    if (userToken) {
        localStorage.clear();
    }
})




