// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');


let token = localStorage.getItem('token');
let userProfile = document.querySelector('#profileContainer');

if (!token) alert('Something went wrong')
else {
    fetch('https://young-earth-66670.herokuapp.com/api/users/details', {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
    .then(res => res.json())
    .then(userData => {
        if (!userData) return false;
        else {
            userProfile.innerHTML = 
                `
                <div class="col-md-12">
                    <div class=" userInfo">
                        <p class="text-center font-weight-bold">
                            First Name: ${userData.firstName}
                        </p>
                        <p class="text-center font-weight-bold">
                            Last Name: ${userData.lastName}
                        </p>
                        <p class="text-center font-weight-bold">
                            Email: ${userData.email}
                        </p>
                        <p class="text-center font-weight-bold">
                            Mobile Number: ${userData.mobileNo}
                        </p>
                        <p class="h3 font-weight-bold">
                            Class History
                        </p>
                        <table class="w-100 my-5">
                            <thead class="d-inline-block thead__content w-100">
                                <tr class="tableHeader">
                                    <th class="mb-lg-5 courseId text-left">Course ID</th>
                                    <th class="mb-lg-5 header__enrolled">Enrolled On</th>
                                    <th class="mb-lg-5 header__status">Status</th>
                                </tr>
                            </thead>
                            <tbody id="tBody" class="d-inline-block w-100">
                                
                            </tbody>
                        </table>
                    </div>
                </div>
                `
            let table = document.querySelector('#tBody');
            userData.enrollments.map(userInfo => {
                if (!userInfo) alert('No such data found!')
                fetch('https://young-earth-66670.herokuapp.com/api/courses')
                .then(result => result.json())
                .then(courseData => {
                    if (courseData.length < 1) alert('There is no such course')
                    courseData.map(course => {
                        if(course._id === userInfo.courseId) {
                            return table.innerHTML += (
                                `
                                <tr class="tableRow">
                                    <td class="py-lg-3 border-top border-bottom w-100 text-left">${course.name}</td>
                                    <td class="py-lg-3 border-top text-centers border-bottom">${userInfo.enrolledOn}</td>
                                    <td class="py-lg-3 border-top border-bottom text-rights">${userInfo.status}</td>
                                </tr>
                                `
                            ) + '<br/>'
                        }
                    }).join('')
                })
            })
            
        }
    })
}



// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);

