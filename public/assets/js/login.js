// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');
let logInUser = document.querySelector('#logInUser');

logInUser.addEventListener('submit', (e) => {
    e.preventDefault();

    let email = document.querySelector('#userEmail').value;
    let password = document.querySelector('#password').value;

    if ((email !== '') || (password !== '')) {
        fetch('https://young-earth-66670.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify({
                'email': email,
                'password': password
            })
        })
        .then(result => result.json())
        .then(data => {
            if (data.accessToken) {
                localStorage.setItem('token', data.accessToken);
    
                fetch('https://young-earth-66670.herokuapp.com/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    // console.log(data)
                    //set the global user state to have properties containing authenticated user's ID and role
                    localStorage.setItem("id", data._id)
                    localStorage.setItem("isAdmin", data.isAdmin)
                    alert('Hello user, Welcome to our Zuitter Page!')
                    window.location.replace("../index.html")
                })
            } else {
                alert('Email or Password you\'ve entered is incorrect');
            }
        })
    }
});


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);