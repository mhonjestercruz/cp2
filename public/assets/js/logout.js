// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

let logout = document.querySelector('#logoutButton')

const clearStorage = () => {
    localStorage.clear();
}


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);


logout.addEventListener('click', () => {
    if (localStorage) {
        window.location.replace('./login.html');
    } else alert('already logout!');
    
})

