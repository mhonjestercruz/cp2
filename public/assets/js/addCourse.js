// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');

let addCourse = document.querySelector('#createCourse');
const myToken = localStorage.getItem('token');
// console.log(addCourse)

addCourse.addEventListener('submit', (e) => {
    e.preventDefault();
    // console.log(e)

    let courseName = document.querySelector('#courseName').value;
    let coursePrice = document.querySelector('#coursePrice').value;
    let courseDescription = document.querySelector('#courseDescription').value;

    if (courseName === '' || coursePrice === '' || courseDescription === '') {
        return alert('Invalid Input');
    } 

    
    fetch('https://young-earth-66670.herokuapp.com/api/courses/checkCourse', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({
            name: courseName
        })
    })
    .then(res => res.json())
    .then(data => {
        if (data) return alert('Duplicate course found!')
        else {
            const myToken = localStorage.getItem('token')
            fetch('https://young-earth-66670.herokuapp.com/api/courses/', {
                method: 'POST',
                headers: {
                    Authorization: `Bearer ${myToken}`,
                    'content-type': 'application/json'
                },
                body: JSON.stringify({
                    name: courseName,
                    description: courseDescription,
                    price: coursePrice
                })
            })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    alert('Course is successfully added');
                    window.location.replace("../pages/courses.html")
                }
            })
        }
    })
})


// function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);
