const myToken = localStorage.getItem('token');
// console.log(myToken)
let params = new URLSearchParams(window.location.search);
let courseIds = params.get('courseId');

fetch(`https://young-earth-66670.herokuapp.com/api/courses/enableCourse/${courseIds}`, {
    method: 'DELETE',
    headers: {
        Authorization: `Bearer ${myToken}`,
        'content-type': 'application/json'
    }
})
.then(res => res.json())
.then(data => {
    if (!data) return alert('Something went wrong')

    return window.location.replace('../pages/courses.html')
})