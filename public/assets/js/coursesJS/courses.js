// declaration of 3 variables for my toggling mobile menu
const menu = document.querySelector('#mobile__menu');
const menuLinks = document.querySelector('.navbar__menu');
const navbarLogo = document.querySelector('.navbar__logo');
let headerButton = document.querySelector('#headerButton')

// declaration of 4 variables to fetch on backend
let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let cardFooter;
let myToken = localStorage.getItem('token');

if (adminUser == 'false' || !adminUser) {
    modalButton.innerHTML = null;
} else {
    modalButton.innerHTML = 
    `
    <div class="">
        <a href="./addCourse.html" class="btn btn-block modalButton">Add Course</a>
    </div>
    `
    
}

//fetch courses from api
fetch('https://young-earth-66670.herokuapp.com/api/courses')
 .then(result => result.json())
 .then(data => {
    //  console.log(data);
    let courseData ;
    if (data.length < 1) {
        courseData = 'No Courses Available'
    } else {

        // if the user is a regular user, display when the course was created and display the button, select course
        

        // else if the user is an admin, display the edit and delete button

        //when the edited button is clicked, it will redirect the user to editCourse.html

        //when the delete button is clicked, the course will be disabled

        courseData = data.map(course => {
            
            if (adminUser == 'true') {
                cardFooter = 
                //elements id is ?courseId=${course._id}
                `
                <div class="row text-center">
                    <div class="col">
                        <a href="./editCourse.html?courseId=${course._id}" class="btn btn-block button__color">Update</a>
                    </div>
                    <div class="col">
                        <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-block button__color2">Disable</a>
                    </div>
                </div>
                `
            } else {
                cardFooter = 
                `
                <div class="col">
                    <a href="./course.html?courseId=${course._id}" class="btn btn-block button__color">Select Course</a>
                </div>
                `
            }
            if (!myToken) cardFooter = 'Top programming Languages now a days'
            return(
                `
                <div class="col-md-6 my-3">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">${course.name}</h5>
                            <p class="card-text text-left">
                                ${course.description}
                            </p>
                            <p class="card-text text-right">
                                ₱ ${course.price}
                            </p>
                        </div>
                        <div class="card-footer">
                            ${cardFooter}
                        </div>
                    </div>	
                </div>
                `
            )
        }).join('')
        // since the collection is an array, we can use join to indicate the separator of each element to replace the comma
        let container = document.querySelector('#coursesContainer')
        container.innerHTML = courseData
    }
 });

 // function for toggling mobile menu
const myToggleBar = ()  => {
    // to toggle the bars in mobile menu
    menu.classList.toggle('is-active');
    // to toggle the navbar menu
    menuLinks.classList.toggle('active');
}
// add event listener to activate the toggler inside the function
menu.addEventListener('click', myToggleBar);



if (!myToken) {
    headerButton.innerHTML = 
    `
    <a href="./login.html" class="header__button">Enroll Now!</a>
    `
} else {
    headerButton.innerHTML = 
    `
    <a href="#coursesContainer" class="header__button">Enroll Now!</a>
    `
}