let adminFalse = localStorage.getItem("isAdmin");
let usedToken = localStorage.getItem('token');
let sampleText = document.querySelector('#sampleText')
let disabledText = document.querySelector('#disableText');
let footer;

if (adminFalse == 'true') {
    fetch('https://young-earth-66670.herokuapp.com/api/courses/isActiveFalse')
    .then(result => result.json())
    .then(data => {
        let courseActiveFalse;
        if (data.length < 1) {
            courseActiveFalse = 'No Courses Available'
        } else {

            

            courseActiveFalse = data.map(courses => {
                footer = 
                `
                <div class="row text-center">
                    <div class="col">
                        <a href="./editCourse.html?courseId=${courses._id}" class="btn btn-block button__color">Update</a>
                    </div>
                    <div class="col">
                        <a href="./enableCourse.html?courseId=${courses._id}" class="btn btn-block button__color3">Enable Course</a>
                    </div>
                </div>
                `
                return(
                    `
                    <div class="col-md-6 my-3">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">${courses.name}</h5>
                                <p class="card-text text-left">
                                    ${courses.description}
                                </p>
                                <p class="card-text text-right">
                                    ₱ ${courses.price}
                                </p>
                            </div>
                            <div class="card-footer">
                                ${footer}
                            </div>
                        </div>	
                    </div>
                    `
                )
            }).join('')
            let isActiveContainer = document.querySelector('#isActiveContainer')
            isActiveContainer.innerHTML = courseActiveFalse;
        }
    })
} else {
    isActiveContainer.style.display = 'none'
}

if (adminFalse == 'true') {
    sampleText.innerHTML = 'Active Courses';
    disabledText.innerHTML = 'Disabled Courses';
} else {
    sampleText.innerHTML = 'Courses to offer';
    disabledText.innerHTML = '';
}